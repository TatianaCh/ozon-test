This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the build folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

Работающее приложение можно открыть по ссылке
https://admiring-tereshkova-997741.netlify.com/

Для получения данных использовался сервис https://jsonplaceholder.typicode.com/
Выполняются запросы для получения поста с указанным id. Перебор Id начинается с номера 90, чтобы не ждать долго до конца, потому что в системе 100 постов. Изменить начальный id можно в конструкторе компонента MainPage, переменная this.currentPostId.