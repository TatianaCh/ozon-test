import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import config from '../../config';
import Counter from '../Counter';
import './MainPage.scss';

class MainPage extends Component {

  constructor(props) {
    super(props);
    this.getInfo = this.getInfo.bind(this);
    this.currentPostId = 90; // because there are 100 posts in service, and we want to finish sometime
    this.state = {
      error: '',
      data: {},
      timeOut: 0,
    };
  }

  componentDidMount() {
    this.getInfo();
  }

  getInfo() {
    this.currentPostId += 1;
    fetch(`${config.apiPrefix}/${this.currentPostId}`)
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else if (response.status === 404) {
          const error = 'There are no more info.';
          throw error;
        } else {
          const error = `Error: ${response.status} - ${response.statusText}`;
          throw error;
        }
      })
      .then((res) => {
        const timeOut = Math.round(Math.random() * 10) * 1000;
        this.setState({
          data: res,
          timeOut, 
        });
        setTimeout(this.getInfo, timeOut);
      })
      .catch((err) => {
        this.setState({
          error: err,
          data: {},
        });
      });
  }

  render() {
    const { error, data, timeOut } = this.state;

    return (
      <div className="content">
        {error && (<div className="error-message">{error}</div>)}
        {!error && (<Counter timevalue={timeOut} />)}
        <div className="data">
          <h3>{data.title}</h3>
          <p>{data.body}</p>
        </div>
      </div>
    );
  }
}

export default MainPage;
