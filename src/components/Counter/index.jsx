import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Counter extends Component {
  static propTypes = {
    timevalue: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);
    this.decreaseCounter = this.decreaseCounter.bind(this);
    this.interval = null;
    this.state = {
      timevalue: 0,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      timevalue: nextProps.timevalue,
    });
    this.decreaseCounter();
  }

  componentWillUnmount() {
    if (this.interval) {
      window.clearInterval(this.interval);
    }
  }

  decreaseCounter() {
    if (this.interval) {
      window.clearInterval(this.interval);
    }
    this.interval = setInterval(() => {
      const value = this.state.timevalue - 1000;
      this.setState({
        timevalue: value >= 0 ? value : 0,
      });
    }, 1000);
  }

  render() {
    const { timevalue } = this.state;
    return (
      <div className="counter">
        We will load next data after <span className="value">{Math.round(timevalue / 1000)}s</span>
      </div>
    );
  }
}

export default Counter;
